export interface Vehicle {
  id: string;
  brand: string;
  model: string;
  registration_number: string;

  vin: string;
  date_of_fabrication: string;

  technical_inspection_exp: string;
  assurance_exp: string;
  is_available: boolean;

  comments: string;

  created_at: string;
  created_by_id: string;
  created_by_user: string;

  updated_at: string;
  updated_by_id: string;
  updated_by_user: string;

  deleted_at: string;
  deleted_by_id: string;
  deleted_by_user: string;

  restored_at: string;
  restored_by_id: string;
  restored_by_user: string;
}

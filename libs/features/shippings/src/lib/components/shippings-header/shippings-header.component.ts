import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { BadgeComponent } from '@jurca-raul/components/badge';
import { ButtonComponent } from '@jurca-raul/components/button';
import { TabNavComponent } from '@jurca-raul/components/tab-nav';
import { RouterLinkService } from '@jurca-raul/services/router';
import { TabNavElement } from 'libs/components/tab-nav/src/lib/domain';

export enum ShippingsTabEnum {
  ACTIVE = 'active',
  COMPLETED = 'completed',
}

@Component({
  selector: 'jr-shippings-header',
  templateUrl: './shippings-header.component.html',
  styleUrls: ['./shippings-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [TabNavComponent, ButtonComponent, BadgeComponent],
})
export class ShippingsHeaderComponent implements OnInit {
  @Input() activeTab?: string;
  @Input({ required: true }) badge: string = '';

  tabs: TabNavElement[] = [];

  constructor(private routerLinkService: RouterLinkService) {}

  ngOnInit(): void {
    this.defineTabs();
  }
  private defineTabs(): void {
    this.tabs = [
      {
        label: 'Active Shippings',
        active: this.activeTab === ShippingsTabEnum.ACTIVE,
        link: this.routerLinkService.routerLink.activeShippings,
        tooltip: 'Active Shippings',
      },
      {
        label: 'Completed Shippings',
        active: this.activeTab === ShippingsTabEnum.COMPLETED,
        link: this.routerLinkService.routerLink.completedShippings,
        tooltip: 'Completed Shippings',
      },
    ];
  }
}

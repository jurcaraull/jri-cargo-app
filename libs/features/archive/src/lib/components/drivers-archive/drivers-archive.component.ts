import { Component, DestroyRef, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgFor, NgIf } from '@angular/common';
import { JrDatePipe } from '@jurca-raul/pipes/date';
import { SupabaseService } from '@jurca-raul/services/supabase';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IconComponent } from '@jurca-raul/components/icon';
import { ButtonComponent } from '@jurca-raul/components/button';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { IconButtonComponent } from '@jurca-raul/components/icon-button';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrService } from 'ngx-toastr';
import { ConfirmModalComponent } from '@jurca-raul/modals/confirm';
import { NgArrayPipesModule } from 'ngx-pipes';
import { InputDirective, InputGroupComponent, SuffixDirective } from '@jurca-raul/components/form';
import { AuthCoreService } from '@jurca-raul/core-auth';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ArchiveTabEnum } from '../../domain';
import { Driver } from '@jurca-raul/domains/driver';
import { ArchiveHeaderComponent } from '../archive-header';
import { TablePreloaderComponent } from '@jurca-raul/components/table-preloader';
import { AddEditDriverModalComponent } from '@jurca-raul/modals/add-edit-view-driver';

@Component({
  selector: 'jr-drivers-archive',
  templateUrl: './drivers-archive.component.html',
  standalone: true,
  imports: [
    ArchiveHeaderComponent,
    NgIf,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    NgFor,
    JrDatePipe,
    IconComponent,
    TooltipModule,
    IconButtonComponent,
    ButtonComponent,
    InputGroupComponent,
    InputDirective,
    SuffixDirective,
    IconComponent,
    NgArrayPipesModule,
    InputGroupComponent,
    TablePreloaderComponent,
  ],
})
export class DriversArchiveComponent implements OnInit {
  readonly noPermissonsMessage = 'You do not have permissions to perform this action';

  archiveTabEnum = ArchiveTabEnum;

  isLoading: boolean = false;
  countedRows?: number;

  drivers: Driver[] = [];
  driver!: Driver;

  permissions: {
    viewArchive?: boolean;
    removeArchive?: boolean;
  } = { viewArchive: false, removeArchive: false };

  searchTerm?: string;
  sortType: string = '';
  sortReverse: boolean = false;

  p: number = 1;
  user: any;

  constructor(
    private destroyRef: DestroyRef,
    private supabaseService: SupabaseService,
    private bsModalService: BsModalService,
    public bsModalRef: BsModalRef,
    private toastr: ToastrService,
    private authCoreService: AuthCoreService // used to get user information
  ) {
    this.loadPermissions();
  }

  ngOnInit(): void {
    this.countRows();
    this.fetchData();
    console.log(this.permissions.viewArchive);
  }

  private loadPermissions() {
    this.authCoreService.getPermissions().subscribe((permissions) => {
      const permissionsObject: Record<string, boolean> = {};
      permissions.forEach((permissions) => {
        const permissionName = permissions.permission_name;
        permissionsObject[permissionName] = true;
      });
      this.permissions = permissionsObject;
    });
  }

  private fetchData() {
    this.isLoading = true;
    setTimeout(() => {
      this.supabaseService.getData('deleted_drivers').then((driversDB) => {
        this.drivers = driversDB;
        this.isLoading = false;
      });
    }, 2000);
  }

  reloadTable() {
    this.drivers = [];
    this.countRows();
    this.fetchData();
  }

  private countRows() {
    this.supabaseService.getData('deleted_drivers').then((driversDB) => {
      this.countedRows = driversDB.length;
    });
  }

  sort(sortColumn: string) {
    if (this.sortType === sortColumn) {
      this.sortReverse = !this.sortReverse;
    } else {
      this.sortType = sortColumn;
      this.sortReverse = false;
    }

    this.drivers.sort((a: any, b: any) => {
      if (a[this.sortType] < b[this.sortType]) {
        return this.sortReverse ? 1 : -1;
      }
      if (a[this.sortType] > b[this.sortType]) {
        return this.sortReverse ? -1 : 1;
      }
      return 0;
    });
  }

  openDriverProfile(driver: Driver) {
    this.bsModalRef = this.bsModalService.show(AddEditDriverModalComponent, {
      class: 'modal-xl',
      initialState: { driver: driver, viewDetails: true },
    });
  }

  restoreDriver(driver: Driver) {
    //should remove the entry from this table and add it to the drivers table
    this.bsModalRef = this.bsModalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: { action: 'Restore', param1: driver.s_name, param2: driver.f_name },
    });

    this.bsModalRef.content.Confirm.subscribe(() => {
      this.authCoreService
        .getUserInfo()
        .pipe(takeUntilDestroyed(this.destroyRef))
        .subscribe({
          next: (user: any) => {
            driver.restored_by_id = user.id;
            driver.restored_by_name = user.name;
            driver.restored_at = new Date().toISOString(); // this can be also handled by supabase

            this.supabaseService.newDriver(driver).then((res) => {
              if (res.error) {
                this.toastr.error(`${res.error?.details}`, 'Driver not restored');
              } else {
                this.supabaseService.deleteDriver('deleted_drivers', driver.id).then((res) => {
                  if (res.error) {
                    this.toastr.error(
                      `${res.error?.details}`,
                      'Driver not was restored but not deleted from the archive'
                    );
                    this.toastr.info('Please contact the System Administrator', 'Info!');
                  } else {
                    this.toastr.success('Driver restored successfully', 'Success!');
                  }

                  this.reloadTable();

                  this.bsModalRef.hide();
                });
                this.toastr.success('Driver restored successfully', 'Success!');
              }
            });
          },
        });
    });
    this.bsModalRef.content.Decline.subscribe(() => {
      this.bsModalRef.hide();
      this.toastr.info('Action canceled', 'Info!');
    });
  }

  deleteDriver(driver: Driver) {
    this.bsModalRef = this.bsModalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: {
        action: 'Delete',
        message: '!!! ATENTION this action is NOT REVERSIBLE. ALL THE DATA WILL BE LOST FOREVER!',
      },
    });

    this.bsModalRef.content.Confirm.subscribe(() => {
      this.supabaseService.removeDriverFromArchive(driver.id).then((res) => {
        if (res.error) {
          this.toastr.error(`${res.error?.details}`, 'Driver not deleted');
        } else {
          this.toastr.success('Driver deleted', 'Success!');
          this.reloadTable();
          this.bsModalRef.hide();
        }
      });
    });

    this.bsModalRef.content.Decline.subscribe(() => {
      this.bsModalRef.hide();
      this.toastr.info('Action canceled', 'Info!');
    });
  }
}

import { Component, DestroyRef, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgFor, NgIf } from '@angular/common';
import { JrDatePipe } from '@jurca-raul/pipes/date';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IconComponent } from '@jurca-raul/components/icon';
import { ButtonComponent } from '@jurca-raul/components/button';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { IconButtonComponent } from '@jurca-raul/components/icon-button';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrService } from 'ngx-toastr';
import { ConfirmModalComponent } from '@jurca-raul/modals/confirm';
import { NgArrayPipesModule } from 'ngx-pipes';
import { InputDirective, InputGroupComponent, SuffixDirective } from '@jurca-raul/components/form';
import { AuthCoreService } from '@jurca-raul/core-auth';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ArchiveTabEnum } from '../../domain';

import { ArchiveHeaderComponent } from '../archive-header';
import { TablePreloaderComponent } from '@jurca-raul/components/table-preloader';

import { Stores } from '@jurca-raul/domains/stores';
import { StoreService } from '@jurca-raul/services/store';
import { AddEditStoreModalComponent } from '@jurca-raul/modals/add-edit-view-stores';

@Component({
  selector: 'jr-store-archive',
  templateUrl: './store-archive.component.html',
  standalone: true,
  imports: [
    ArchiveHeaderComponent,
    NgIf,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    NgFor,
    JrDatePipe,
    IconComponent,
    TooltipModule,
    IconButtonComponent,
    ButtonComponent,
    InputGroupComponent,
    InputDirective,
    SuffixDirective,
    IconComponent,
    NgArrayPipesModule,
    InputGroupComponent,
    TablePreloaderComponent,
  ],
})
export class StoresArchiveComponent implements OnInit {
  readonly noPermissonsMessage = 'You do not have permissions to perform this action';

  archiveTabEnum = ArchiveTabEnum;

  isLoading: boolean = false;
  countedRows?: number;

  sortType: string = '';
  sortReverse: boolean = false;
  searchTerm?: string;

  stores: Stores[] = [];

  permissions: {
    viewArchive?: boolean;
    removeArchive?: boolean;
  } = { viewArchive: false, removeArchive: false };

  p: number = 1;
  user: any;

  constructor(
    private destroyRef: DestroyRef,
    private storeService: StoreService,
    private bsModalService: BsModalService,
    public bsModalRef: BsModalRef,
    private toastr: ToastrService,
    private authCoreService: AuthCoreService // used to get user information
  ) {
    this.loadPermissions();
  }

  ngOnInit(): void {
    this.countRows();
    this.fetchData();
  }

  private loadPermissions() {
    this.authCoreService.getPermissions().subscribe((permissions) => {
      const permissionsObject: Record<string, boolean> = {};
      permissions.forEach((permissions) => {
        const permissionName = permissions.permission_name;
        permissionsObject[permissionName] = true;
      });
      this.permissions = permissionsObject;
    });
  }

  private fetchData() {
    this.isLoading = true;
    setTimeout(() => {
      this.storeService.getStoresFromArchive().then((storesArchive) => {
        this.stores = storesArchive;
        this.isLoading = false;
      });
    }, 2000);
  }

  reloadTable() {
    this.stores = [];
    this.countRows();
    this.fetchData();
  }

  sort(sortColumn: string) {
    if (this.sortType === sortColumn) {
      this.sortReverse = !this.sortReverse;
    } else {
      this.sortType = sortColumn;
      this.sortReverse = false;
    }

    this.stores.sort((a: any, b: any) => {
      if (a[this.sortType] < b[this.sortType]) {
        return this.sortReverse ? 1 : -1;
      }
      if (a[this.sortType] > b[this.sortType]) {
        return this.sortReverse ? -1 : 1;
      }
      return 0;
    });
  }

  private async countRows() {
    this.storeService.getStoresFromArchive().then((storesArchive) => {
      this.countedRows = storesArchive.length;
    });
  }

  restoreStore(store: Stores) {
    this.bsModalRef = this.bsModalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: { action: 'Restore' },
    });

    this.bsModalRef.content.Confirm.subscribe(() => {
      this.authCoreService
        .getUserInfo()
        .pipe(takeUntilDestroyed(this.destroyRef))
        .subscribe({
          next: (user: any) => {
            store.restored_by_id = user.id;
            store.restored_by_user = user.name;
            store.restored_at = new Date().toISOString();

            this.storeService.newsStore(store).then((res) => {
              if (res.error) {
                this.toastr.error(`${res.error?.details}`, 'Store not restored');
              } else {
                this.storeService.removeStoreFromArchive(store.id).then((res) => {
                  if (res.error) {
                    this.toastr.error(`${res.error?.details}`, 'Store was restored but not deleted from the archive');
                    this.toastr.info('Please contact the System Administrator', 'Info!');
                  } else {
                    this.toastr.success('Store restored successfully', 'Success!');
                  }

                  this.reloadTable();
                  this.bsModalRef.hide();
                });
                this.toastr.success('Store restored successfully', 'Success!');
                this.reloadTable();
                this.bsModalRef.hide();
              }
            });
          },
        });
    });
    this.bsModalRef.content.Decline.subscribe(() => {
      this.bsModalRef.hide();
      this.toastr.info('Action canceled', 'Info!');
    });
  }

  openStoreDetails(store: Stores) {
    this.bsModalRef = this.bsModalService.show(AddEditStoreModalComponent, {
      class: 'modal-xl',
      initialState: { store: store, viewDetails: true },
    });
  }

  deleteStore(store: Stores) {
    this.bsModalRef = this.bsModalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: {
        action: 'Delete',
        message: '!!! ATENTION this action is NOT REVERSIBLE. ALL THE DATA WILL BE LOST FOREVER!',
      },
    });

    this.bsModalRef.content.Confirm.subscribe(() => {
      this.storeService.removeStoreFromArchive(store.id).then((res) => {
        if (res.error) {
          this.toastr.error(`${res.error?.details}`, 'Store not deleted');
        } else {
          this.toastr.success('Store deleted', 'Success!');
          this.reloadTable();
          this.bsModalRef.hide();
        }
      });
    });

    this.bsModalRef.content.Decline.subscribe(() => {
      this.bsModalRef.hide();
      this.toastr.info('Action canceled', 'Info!');
    });
  }
}

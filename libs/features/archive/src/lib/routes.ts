import { Routes } from '@angular/router';
import { DriversArchiveComponent } from './components/drivers-archive';
import { VehiclesArchiveComponent } from './components/vehicles-archive';
import { DeliveriesArchiveComponent } from './components/deliveries-archive';
import { StoresArchiveComponent } from './components/store-archive';

export const routes: Routes = [
  { path: 'drivers', component: DriversArchiveComponent, data: { title: 'Archive - Drivers' } },
  { path: 'vehicles', component: VehiclesArchiveComponent, data: { title: 'Archive - Vehicles' } },
  { path: 'deliveries', component: DeliveriesArchiveComponent, data: { title: 'Archive - Deliveries' } },
  { path: 'stores', component: StoresArchiveComponent, data: { title: 'Archive - Stores' } },
  { path: '', redirectTo: 'vehicles', pathMatch: 'full', data: { title: 'Archive - Vehicles' } },
];

export default routes;

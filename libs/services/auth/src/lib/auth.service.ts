import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService as Auth0Service } from '@auth0/auth0-angular';
import { User } from '@auth0/auth0-spa-js';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user$ = new BehaviorSubject<User | null>(null);

  constructor(private auth: Auth0Service) {}

  getUser(): Observable<User> {
    return this.auth.user$.pipe(
      filter((user: User | null | undefined): user is User => user !== null && user !== undefined),
      tap((user) => this.user$.next(user))
    );
  }

  getUserName(): Observable<string | null> {
    return this.user$.pipe(
      map((user) => (user ? user.name : null)),
      filter((name): name is string | null => name !== undefined)
    );
  }
}

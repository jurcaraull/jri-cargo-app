import { Injectable } from '@angular/core';
import { supabaseDb } from '@jurca-raul/environment';

@Injectable({
  providedIn: 'root',
})
export class SupabaseService {
  changes: any;

  // generic function for getting data from supabase table
  getData(table: string) {
    return supabaseDb
      .from(table)
      .select('*')
      .then((res: any) => res.data);
  }

  getAvailableData(table: string) {
    return supabaseDb
      .from(table)
      .select('*')
      .eq('is_available', true)
      .then((res: any) => res.data);
  }

  getAvailablevehicles() {
    return supabaseDb
      .from('vehicles')
      .select('*')
      .eq('is_available', true)
      .then((res: any) => res.data);
  }
  // methods for drivers table
  newDriver(driver: any) {
    return supabaseDb.from('drivers').insert([driver]).select();
  }

  updateDriver(driver: any, id: string) {
    return supabaseDb.from('drivers').update(driver).eq('id', id).select();
  }
  deleteDriver(table: string, id: string) {
    return supabaseDb.from(table).delete().eq('id', id).select();
  }

  moveDriverToArchive(driver: any) {
    return supabaseDb.from('deleted_drivers').insert([driver]).select();
  }

  removeDriverFromArchive(id: string) {
    return supabaseDb.from('deleted_drivers').delete().eq('id', id).select();
  }

  // a try to use a trigger to update the is_available field on drivers table
  markDriverUnavailableOnNewShipping(id: string, isAvailable: boolean) {
    return supabaseDb.from('drivers').update({ is_available: isAvailable }).eq('id', id).select();
  }

  makeVehiclesUnavailableOnNewShipping(id: string, isAvailable: boolean) {
    return supabaseDb.from('vehicles').update({ is_available: isAvailable }).eq('id', id).select();
  }

  makeStoreUnavailableOnNewShipping(id: string, isAvailable: boolean) {
    return supabaseDb.from('stores').update({ is_available: isAvailable }).eq('id', id).select();
  }

  // getAvailableDrivers() {
  //   return supabaseDb
  //     .from('drivers')
  //     .select('*')
  //     .eq('is_available', true)
  //     .then((res: any) => res.data);
  // }
}

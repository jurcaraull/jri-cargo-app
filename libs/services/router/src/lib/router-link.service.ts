import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class RouterLinkService {
  routerLink = {
    // MAIN
    drivers: ['drivers'],
    deliveries: ['deliveries'],
    vehicles: ['vehicles'],
    stores: ['stores'],
    archive: ['archive'],
    administration: ['administration'],

    // SETTINGS
    vehiclesArchive: ['/', 'archive', 'vehicles'],
    driversArchive: ['/', 'archive', 'drivers'],
    deliveriesArchive: ['/', 'archive', 'deliveries'],
    storesArchive: ['/', 'archive', 'stores'],

    // HISTORY
    activeShippings: ['/', 'deliveries', 'active'],
    completedShippings: ['/', 'deliveries', 'completed'],
  };
}

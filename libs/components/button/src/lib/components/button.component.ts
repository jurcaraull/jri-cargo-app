import { NgClass, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import {
  ButtonFlavour,
  ButtonFlavours,
  ButtonFontStyle,
  ButtonFontStyles,
  ButtonFontWeight,
  ButtonFontWeights,
  ButtonSize,
  ButtonSizes,
  ButtonType,
  ButtonVariant,
  ButtonVariants,
  IconPosition,
  IconPositions,
} from '../constants';
import { Icon, IconAnimation, IconComponent, IconSize, IconStyle } from '@jurca-raul/components/icon';

@Component({
  selector: 'jr-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [NgClass, NgIf, IconComponent],
})
export class ButtonComponent {
  @Input({ required: true }) text: string = 'Button';
  @Input() icon: Icon = 'UNSET';
  @Input() iconSize: IconSize = 'MEDIUM';
  @Input() iconPosition: IconPosition = 'LEFT';
  @Input() iconAnimation: IconAnimation = 'UNSET';
  @Input() iconStyle: IconStyle = 'ICON';
  @Input() variant: ButtonVariant = 'SOLID';
  @Input() flavour: ButtonFlavour = 'PRIMARY';
  @Input() type: ButtonType = 'BUTTON';
  @Input() size: ButtonSize = 'MEDIUM';
  @Input() fontWeight: ButtonFontWeight = 'BOLD';
  @Input() fontStyle: ButtonFontStyle = 'NORMAL';
  @Input() disabled: boolean = false;
  @Input() isFullWidth: boolean = false;
  @Input() isTruncated: boolean = false;
  @Input() hasMarginLeft: boolean = false;
  @Input() hasMarginRight: boolean = false;
  @Input() hasMarginTop: boolean = false;
  @Input() hasMarginBottom: boolean = false;
  @Input() withDarkBackground: boolean = false;

  buttonSizes = ButtonSizes;
  buttonFlavours = ButtonFlavours;
  buttonFontWeights = ButtonFontWeights;
  buttonFontStyles = ButtonFontStyles;
  buttonVariants = ButtonVariants;
  iconPositions = IconPositions;
}

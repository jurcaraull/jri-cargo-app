type ButtonVariant = 'SOLID' | 'OUTLINED' | 'GHOST' | 'TEXT_BUTTON';

const ButtonVariants: Record<ButtonVariant, string> = {
  SOLID: 'solid',
  OUTLINED: 'outlined',
  GHOST: 'ghost',
  TEXT_BUTTON: 'text-button',
} as const;

type ButtonFlavour = 'PRIMARY' | 'SECONDARY' | 'DANGER' | 'ADMIN' | 'SUCCESS';

const ButtonFlavours: Record<ButtonFlavour, string> = {
  PRIMARY: 'primary',
  SECONDARY: 'secondary',
  DANGER: 'danger',
  ADMIN: 'admin',
  SUCCESS: 'success',
} as const;

type ButtonType = 'BUTTON' | 'SUBMIT' | 'RESET';

const ButtonTypes: Record<ButtonType, string> = {
  BUTTON: 'button',
  SUBMIT: 'submit',
  RESET: 'reset',
} as const;

type ButtonSize = 'EXTRA_LARGE' | 'LARGE' | 'MEDIUM' | 'SMALL' | 'EXTRA_SMALL';

const ButtonSizes: Record<ButtonSize, string> = {
  EXTRA_LARGE: 'extra_large',
  LARGE: 'large',
  MEDIUM: 'medium',
  SMALL: 'small',
  EXTRA_SMALL: 'extra_small',
} as const;

type ButtonFontWeight = 'NORMAL' | 'MEDIUM' | 'BOLD';

const ButtonFontWeights: Record<ButtonFontWeight, string> = {
  NORMAL: 'font-weight-normal',
  MEDIUM: 'font-weight-medium',
  BOLD: 'font-weight-bold',
};

type ButtonFontStyle = 'NORMAL' | 'ITALIC';

const ButtonFontStyles: Record<ButtonFontStyle, string> = {
  NORMAL: '',
  ITALIC: 'font-style-italic',
};

type IconPosition = 'LEFT' | 'RIGHT';

const IconPositions: Record<IconPosition, string> = {
  LEFT: 'left',
  RIGHT: 'right',
};

export {
  ButtonFlavour,
  ButtonFlavours,
  ButtonFontStyle,
  ButtonFontStyles,
  ButtonFontWeight,
  ButtonFontWeights,
  ButtonSize,
  ButtonSizes,
  ButtonType,
  ButtonTypes,
  ButtonVariant,
  ButtonVariants,
  IconPosition,
  IconPositions,
};

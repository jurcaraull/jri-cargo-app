import { NgClass } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
  Icon,
  IconAnimation,
  IconComponent,
  IconStyle,
} from '@jurca-raul/components/icon';
import {
  IconButtonSize,
  IconButtonVariant,
  IconButtonFlavour,
  IconButtonType,
  IconButtonSizes,
  IconButtonVariants,
  IconButtonFlavours,
} from '../domain';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'jr-icon-button',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [NgClass, IconComponent],
})
export class IconButtonComponent {
  defaultSize: IconButtonSize = 'SMALL';
  defaultVariant: IconButtonVariant = 'SOLID';
  defaultFlavour: IconButtonFlavour = 'PRIMARY';
  defaultType: IconButtonType = 'BUTTON';

  @Input({ required: true }) icon: Icon = 'UNSET';
  @Input() iconStyle: IconStyle = 'ICON';
  @Input() iconAnimation: IconAnimation = 'UNSET';
  @Input() size: IconButtonSize = this.defaultSize;
  @Input() variant: IconButtonVariant = this.defaultVariant;
  @Input() flavour: IconButtonFlavour = this.defaultFlavour;
  @Input() type: IconButtonType = this.defaultType;
  @Input() disabled: boolean = false;
  @Input() isRound: boolean = false;
  @Input() withDarkBackground: boolean = false;

  iconButtonSizes = IconButtonSizes;
  iconButtonVariants = IconButtonVariants;
  iconButtonFlavours = IconButtonFlavours;
}

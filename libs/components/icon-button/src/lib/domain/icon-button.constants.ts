type IconButtonVariant = 'SOLID' | 'OUTLINED' | 'GHOST' | 'SIMPLE';

const IconButtonVariants: Record<IconButtonVariant, string> = {
  SOLID: 'solid',
  OUTLINED: 'outlined',
  GHOST: 'ghost',
  SIMPLE: 'simple',
} as const;

type IconButtonFlavour = 'PRIMARY' | 'SECONDARY' | 'DANGER' | 'ADMIN' | 'SUCCESS' | 'CLASIC';

const IconButtonFlavours: Record<IconButtonFlavour, string> = {
  PRIMARY: 'primary',
  SECONDARY: 'secondary',
  DANGER: 'danger',
  ADMIN: 'admin',
  SUCCESS: 'success',
  CLASIC: 'clasic',
} as const;

type IconButtonType = 'BUTTON' | 'SUBMIT' | 'RESET';

const IconButtonTypes: Record<IconButtonType, string> = {
  BUTTON: 'button',
  SUBMIT: 'submit',
  RESET: 'reset',
} as const;

type IconButtonSize = 'LARGE' | 'MEDIUM' | 'SMALL';

const IconButtonSizes: Record<IconButtonSize, string> = {
  LARGE: 'large',
  MEDIUM: 'medium',
  SMALL: 'small',
} as const;

export {
  IconButtonFlavour,
  IconButtonFlavours,
  IconButtonSize,
  IconButtonSizes,
  IconButtonType,
  IconButtonTypes,
  IconButtonVariant,
  IconButtonVariants,
};

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import {
  FormFieldLayout,
  FormFieldLayoutType,
  FormFieldSize,
  FormFieldSizeType,
} from './constants';
import {
  InputDirective,
  PrefixDirective,
  SuffixDirective,
} from '../directives';

@Component({
  selector: 'jr-form-field-container',
  templateUrl: './form-field-container.component.html',
  styleUrls: ['./form-field-container.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [InputDirective, PrefixDirective, SuffixDirective],
})
export class FormFieldContainerComponent {
  @Input() layout: FormFieldLayoutType = FormFieldLayout.VERTICAL;
  @Input() size: FormFieldSizeType = FormFieldSize.NORMAL;

  @Input() hasError = false;
  @Input() hasWarning = false;

  @Input() hasSuffix = false;
  @Input() hasPrefix = false;
  @Input() hasLabel = true;

  formFieldLayout = FormFieldLayout;
  formFieldSize = FormFieldSize;
}

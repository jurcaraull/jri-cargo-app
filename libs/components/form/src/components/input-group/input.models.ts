export const InputTypes = {
  Text: 'text',
  Email: 'email',
  Password: 'password',
  Search: 'search',
} as const;

export type InputType = (typeof InputTypes)[keyof typeof InputTypes];

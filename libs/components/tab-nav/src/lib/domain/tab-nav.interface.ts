export interface TabNavElement {
  label: string;
  link: string[];
  active: boolean;
  disabled?: boolean;
  tooltip?: string;
}

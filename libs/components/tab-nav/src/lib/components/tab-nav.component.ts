import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TabNavElement } from '../domain';
import { NgClass, NgFor } from '@angular/common';
import { RouterLink } from '@angular/router';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

@Component({
  selector: 'jr-tab-nav',
  templateUrl: './tab-nav.component.html',
  styleUrls: ['./tab-nav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,

  standalone: true,
  imports: [NgFor, RouterLink, NgClass, TooltipModule],
})
export class TabNavComponent {
  @Input() tabs?: TabNavElement[];
}

import { DestroyRef, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService, User } from '@auth0/auth0-angular';
import { Observable, concatMap, map, of, take, tap } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

interface UserProfile {
  id: string;
  name: string;
  email: string;
  email_verified: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class AuthCoreService {
  accessToken: string = '';
  permission: any[] = [];
  readonly noPermissonsMessage = 'You do not have permissions to perform this action';

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    private destroyRef: DestroyRef
  ) {}

  //getUserInfo() returns an object with the user profile information
  getUserInfo(): Observable<User> {
    return this.auth.user$.pipe(
      map((user: User) => ({
        id: user.sub,
        name: user.name,
        email: user.email,
        email_verified: user.email_verified,
      }))
    );
  }

  getSecuredData() {
    const accessToken = this.auth.getAccessTokenSilently(); // or this.auth.idToken$;
    const headers = new HttpHeaders({
      Authorization: `Bearer ${accessToken}`,
    });

    return this.http.get('https://rauljurca.eu.auth0.com/api/cargo-app', { headers });
  }

  userInfo() {
    return this.http
      .get('https://rauljurca.eu.auth0.com/userinfo')
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((data) => {
        console.log(data);
      });
  }

  getPermissions(): Observable<any[]> {
    return this.auth.user$.pipe(
      takeUntilDestroyed(this.destroyRef),
      concatMap((user) => {
        if (!user) {
          return;
        }

        // Use HttpClient to make the call
        const url = `https://rauljurca.eu.auth0.com/api/v2/users/${user.sub}/permissions`;

        // Include the access token in the request headers
        this.auth.getAccessTokenSilently().subscribe((token) => {
          this.accessToken = token;
          console.log('accessToken:', this.accessToken);
        });
        const headers = new HttpHeaders({
          Accept: 'application/json',
          Authorization: `Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InJPN1VnU002QTJ1c2JCM0VpcmJnYiJ9.eyJpc3MiOiJodHRwczovL3JhdWxqdXJjYS5ldS5hdXRoMC5jb20vIiwic3ViIjoiV3hFZWVjQnZNemdsNlhwUEw3UE9wY21raEYwS0tsaWtAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vcmF1bGp1cmNhLmV1LmF1dGgwLmNvbS9hcGkvdjIvIiwiaWF0IjoxNzA3OTE1MjAzLCJleHAiOjE3MTA1MDcyMDMsImF6cCI6Ild4RWVlY0J2TXpnbDZYcFBMN1BPcGNta2hGMEtLbGlrIiwic2NvcGUiOiJyZWFkOmNsaWVudF9ncmFudHMgY3JlYXRlOmNsaWVudF9ncmFudHMgZGVsZXRlOmNsaWVudF9ncmFudHMgdXBkYXRlOmNsaWVudF9ncmFudHMgcmVhZDp1c2VycyB1cGRhdGU6dXNlcnMgZGVsZXRlOnVzZXJzIGNyZWF0ZTp1c2VycyByZWFkOnVzZXJzX2FwcF9tZXRhZGF0YSB1cGRhdGU6dXNlcnNfYXBwX21ldGFkYXRhIGRlbGV0ZTp1c2Vyc19hcHBfbWV0YWRhdGEgY3JlYXRlOnVzZXJzX2FwcF9tZXRhZGF0YSByZWFkOnVzZXJfY3VzdG9tX2Jsb2NrcyBjcmVhdGU6dXNlcl9jdXN0b21fYmxvY2tzIGRlbGV0ZTp1c2VyX2N1c3RvbV9ibG9ja3MgY3JlYXRlOnVzZXJfdGlja2V0cyByZWFkOmNsaWVudHMgdXBkYXRlOmNsaWVudHMgZGVsZXRlOmNsaWVudHMgY3JlYXRlOmNsaWVudHMgcmVhZDpjbGllbnRfa2V5cyB1cGRhdGU6Y2xpZW50X2tleXMgZGVsZXRlOmNsaWVudF9rZXlzIGNyZWF0ZTpjbGllbnRfa2V5cyByZWFkOmNvbm5lY3Rpb25zIHVwZGF0ZTpjb25uZWN0aW9ucyBkZWxldGU6Y29ubmVjdGlvbnMgY3JlYXRlOmNvbm5lY3Rpb25zIHJlYWQ6cmVzb3VyY2Vfc2VydmVycyB1cGRhdGU6cmVzb3VyY2Vfc2VydmVycyBkZWxldGU6cmVzb3VyY2Vfc2VydmVycyBjcmVhdGU6cmVzb3VyY2Vfc2VydmVycyByZWFkOmRldmljZV9jcmVkZW50aWFscyB1cGRhdGU6ZGV2aWNlX2NyZWRlbnRpYWxzIGRlbGV0ZTpkZXZpY2VfY3JlZGVudGlhbHMgY3JlYXRlOmRldmljZV9jcmVkZW50aWFscyByZWFkOnJ1bGVzIHVwZGF0ZTpydWxlcyBkZWxldGU6cnVsZXMgY3JlYXRlOnJ1bGVzIHJlYWQ6cnVsZXNfY29uZmlncyB1cGRhdGU6cnVsZXNfY29uZmlncyBkZWxldGU6cnVsZXNfY29uZmlncyByZWFkOmhvb2tzIHVwZGF0ZTpob29rcyBkZWxldGU6aG9va3MgY3JlYXRlOmhvb2tzIHJlYWQ6YWN0aW9ucyB1cGRhdGU6YWN0aW9ucyBkZWxldGU6YWN0aW9ucyBjcmVhdGU6YWN0aW9ucyByZWFkOmVtYWlsX3Byb3ZpZGVyIHVwZGF0ZTplbWFpbF9wcm92aWRlciBkZWxldGU6ZW1haWxfcHJvdmlkZXIgY3JlYXRlOmVtYWlsX3Byb3ZpZGVyIGJsYWNrbGlzdDp0b2tlbnMgcmVhZDpzdGF0cyByZWFkOmluc2lnaHRzIHJlYWQ6dGVuYW50X3NldHRpbmdzIHVwZGF0ZTp0ZW5hbnRfc2V0dGluZ3MgcmVhZDpsb2dzIHJlYWQ6bG9nc191c2VycyByZWFkOnNoaWVsZHMgY3JlYXRlOnNoaWVsZHMgdXBkYXRlOnNoaWVsZHMgZGVsZXRlOnNoaWVsZHMgcmVhZDphbm9tYWx5X2Jsb2NrcyBkZWxldGU6YW5vbWFseV9ibG9ja3MgdXBkYXRlOnRyaWdnZXJzIHJlYWQ6dHJpZ2dlcnMgcmVhZDpncmFudHMgZGVsZXRlOmdyYW50cyByZWFkOmd1YXJkaWFuX2ZhY3RvcnMgdXBkYXRlOmd1YXJkaWFuX2ZhY3RvcnMgcmVhZDpndWFyZGlhbl9lbnJvbGxtZW50cyBkZWxldGU6Z3VhcmRpYW5fZW5yb2xsbWVudHMgY3JlYXRlOmd1YXJkaWFuX2Vucm9sbG1lbnRfdGlja2V0cyByZWFkOnVzZXJfaWRwX3Rva2VucyBjcmVhdGU6cGFzc3dvcmRzX2NoZWNraW5nX2pvYiBkZWxldGU6cGFzc3dvcmRzX2NoZWNraW5nX2pvYiByZWFkOmN1c3RvbV9kb21haW5zIGRlbGV0ZTpjdXN0b21fZG9tYWlucyBjcmVhdGU6Y3VzdG9tX2RvbWFpbnMgdXBkYXRlOmN1c3RvbV9kb21haW5zIHJlYWQ6ZW1haWxfdGVtcGxhdGVzIGNyZWF0ZTplbWFpbF90ZW1wbGF0ZXMgdXBkYXRlOmVtYWlsX3RlbXBsYXRlcyByZWFkOm1mYV9wb2xpY2llcyB1cGRhdGU6bWZhX3BvbGljaWVzIHJlYWQ6cm9sZXMgY3JlYXRlOnJvbGVzIGRlbGV0ZTpyb2xlcyB1cGRhdGU6cm9sZXMgcmVhZDpwcm9tcHRzIHVwZGF0ZTpwcm9tcHRzIHJlYWQ6YnJhbmRpbmcgdXBkYXRlOmJyYW5kaW5nIGRlbGV0ZTpicmFuZGluZyByZWFkOmxvZ19zdHJlYW1zIGNyZWF0ZTpsb2dfc3RyZWFtcyBkZWxldGU6bG9nX3N0cmVhbXMgdXBkYXRlOmxvZ19zdHJlYW1zIGNyZWF0ZTpzaWduaW5nX2tleXMgcmVhZDpzaWduaW5nX2tleXMgdXBkYXRlOnNpZ25pbmdfa2V5cyByZWFkOmxpbWl0cyB1cGRhdGU6bGltaXRzIGNyZWF0ZTpyb2xlX21lbWJlcnMgcmVhZDpyb2xlX21lbWJlcnMgZGVsZXRlOnJvbGVfbWVtYmVycyByZWFkOmVudGl0bGVtZW50cyByZWFkOmF0dGFja19wcm90ZWN0aW9uIHVwZGF0ZTphdHRhY2tfcHJvdGVjdGlvbiByZWFkOm9yZ2FuaXphdGlvbnNfc3VtbWFyeSBjcmVhdGU6YXV0aGVudGljYXRpb25fbWV0aG9kcyByZWFkOmF1dGhlbnRpY2F0aW9uX21ldGhvZHMgdXBkYXRlOmF1dGhlbnRpY2F0aW9uX21ldGhvZHMgZGVsZXRlOmF1dGhlbnRpY2F0aW9uX21ldGhvZHMgcmVhZDpvcmdhbml6YXRpb25zIHVwZGF0ZTpvcmdhbml6YXRpb25zIGNyZWF0ZTpvcmdhbml6YXRpb25zIGRlbGV0ZTpvcmdhbml6YXRpb25zIGNyZWF0ZTpvcmdhbml6YXRpb25fbWVtYmVycyByZWFkOm9yZ2FuaXphdGlvbl9tZW1iZXJzIGRlbGV0ZTpvcmdhbml6YXRpb25fbWVtYmVycyBjcmVhdGU6b3JnYW5pemF0aW9uX2Nvbm5lY3Rpb25zIHJlYWQ6b3JnYW5pemF0aW9uX2Nvbm5lY3Rpb25zIHVwZGF0ZTpvcmdhbml6YXRpb25fY29ubmVjdGlvbnMgZGVsZXRlOm9yZ2FuaXphdGlvbl9jb25uZWN0aW9ucyBjcmVhdGU6b3JnYW5pemF0aW9uX21lbWJlcl9yb2xlcyByZWFkOm9yZ2FuaXphdGlvbl9tZW1iZXJfcm9sZXMgZGVsZXRlOm9yZ2FuaXphdGlvbl9tZW1iZXJfcm9sZXMgY3JlYXRlOm9yZ2FuaXphdGlvbl9pbnZpdGF0aW9ucyByZWFkOm9yZ2FuaXphdGlvbl9pbnZpdGF0aW9ucyBkZWxldGU6b3JnYW5pemF0aW9uX2ludml0YXRpb25zIGRlbGV0ZTpwaG9uZV9wcm92aWRlcnMgY3JlYXRlOnBob25lX3Byb3ZpZGVycyByZWFkOnBob25lX3Byb3ZpZGVycyB1cGRhdGU6cGhvbmVfcHJvdmlkZXJzIGRlbGV0ZTpwaG9uZV90ZW1wbGF0ZXMgY3JlYXRlOnBob25lX3RlbXBsYXRlcyByZWFkOnBob25lX3RlbXBsYXRlcyB1cGRhdGU6cGhvbmVfdGVtcGxhdGVzIGNyZWF0ZTplbmNyeXB0aW9uX2tleXMgcmVhZDplbmNyeXB0aW9uX2tleXMgdXBkYXRlOmVuY3J5cHRpb25fa2V5cyBkZWxldGU6ZW5jcnlwdGlvbl9rZXlzIHJlYWQ6Y2xpZW50X2NyZWRlbnRpYWxzIGNyZWF0ZTpjbGllbnRfY3JlZGVudGlhbHMgdXBkYXRlOmNsaWVudF9jcmVkZW50aWFscyBkZWxldGU6Y2xpZW50X2NyZWRlbnRpYWxzIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.ZqZXX7yfoTEHT3a_tQ1FLCZZqWnGgf_QAJ506tHg93akklnanxZvUUaSIvsF3f-Ywg7Ioiym6GlyCZbokTsxWGClthl6A4AO3vPgStYyTJSR8-p8IGDKAfeJf5HIrOW_1Sue1jxinDENHBQ5dixuViI70zFiNbN8-nKlqUC3Pi98Fbwr2mWWIM-l-I-WXI_7Frli606LwHz_pGyj22rpFUJXWy61QqcoFO5ZuoNB4OTm4PXM_rtTZ5YHp6jEmGgp4DwJqjLjgoYcZgAytkK_18QUpZDX-6K7U_OvIIfT7nu_5hi9lUtefndowmEfvCZ2l8_Z9_lCwvuWY6ZxcXwUzg`,
        });

        return this.http.get(encodeURI(url), { headers });
      }),
      map((user: any) => user),
      tap((meta) => {
        this.permission = meta;
        this.permission = this.permission.map((permission: any) => permission.permission_name); //creates an array with permissions  names
        console.log(' this.permission:', this.permission);
      })
    );
  }
}

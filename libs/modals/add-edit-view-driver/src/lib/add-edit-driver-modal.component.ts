import { Component, DestroyRef, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { SupabaseService } from '@jurca-raul/services/supabase';
import { DatePipe, NgFor, NgIf } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from '@jurca-raul/modals/confirm';
import { ToastrService } from 'ngx-toastr';
import { ModalComponent } from '@jurca-raul/modals/modal';
import { ButtonComponent } from '@jurca-raul/components/button';
import { InputDirective, InputGroupComponent } from '@jurca-raul/components/form';
import { AuthCoreService } from '@jurca-raul/core-auth';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Driver } from '@jurca-raul/domains/driver';
import { DropdownComponent, DropdownOption } from '@jurca-raul/components/dropdown';
import { jsPDF } from 'jspdf';

@Component({
  templateUrl: './add-edit-driver-modal.component.html',
  styleUrls: ['./add-edit-driver-modal.component.scss'],
  standalone: true,
  providers: [DatePipe],
  imports: [
    NgIf,
    NgFor,
    ReactiveFormsModule,
    FormsModule,
    ModalComponent,
    ButtonComponent,
    InputGroupComponent,
    InputDirective,
    BsDatepickerModule,
    DropdownComponent,
  ],
})
export class AddEditDriverModalComponent {
  @Output() tableRefresh: EventEmitter<any> = new EventEmitter();

  viewDetails: boolean = false;
  isSaving: boolean = false;

  newDriverform: FormGroup = new FormGroup({});

  drivers: Driver[] | null = [];
  driver!: Driver;
  label: string = 'Select an option';

  idTypes: DropdownOption[] = [
    { optionValue: 'CI', displayName: 'CI' },
    { optionValue: 'BI', displayName: 'BI' },
    { optionValue: 'Pasaport', displayName: 'Pasaport' },
  ];

  user: any;

  // datepicker configurations
  bsDateConfig: Partial<BsDatepickerModule> = {
    dateInputFormat: 'DD/MM/YYYY',
    containerClass: 'theme-dark-blue',
    isAnimated: true,
    adaptivePosition: true,
  };
  minDateCheckIn?: Date;

  modalRef?: BsModalRef;
  message?: string;

  constructor(
    private destroyRef: DestroyRef,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private modalService: BsModalService,
    public bsModalRef: BsModalRef,
    private supabaseService: SupabaseService,
    private toastr: ToastrService,
    private authCoreService: AuthCoreService
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.newDriverform = this.formBuilder.group({
      s_name: new FormControl(
        { value: this.driver ? this.driver.s_name : null, disabled: this.viewDetails },
        Validators.required
      ),
      f_name: new FormControl(
        { value: this.driver ? this.driver.f_name : null, disabled: this.viewDetails },
        Validators.required
      ),
      date_of_birth: new FormControl(
        {
          value: this.driver ? this.datePipe.transform(this.driver.date_of_birth, 'yyyy/MM/dd') : null,
          disabled: this.viewDetails,
        },
        Validators.required
      ),

      phone: new FormControl(
        { value: this.driver ? this.driver.phone : null, disabled: this.viewDetails },
        Validators.required
      ),
      email: new FormControl(
        { value: this.driver ? this.driver.email : null, disabled: this.viewDetails },
        Validators.required
      ),

      city: new FormControl(
        { value: this.driver ? this.driver.city : null, disabled: this.viewDetails },
        Validators.required
      ),
      county: new FormControl(
        { value: this.driver ? this.driver.county : null, disabled: this.viewDetails },
        Validators.required
      ),
      street: new FormControl(
        { value: this.driver ? this.driver.street : null, disabled: this.viewDetails },
        Validators.required
      ),
      street_number: new FormControl(
        { value: this.driver ? this.driver.street_number : null, disabled: this.viewDetails },
        Validators.required
      ),

      id_document_type: new FormControl(
        { value: this.driver ? this.driver.id_document_type : null, disabled: this.viewDetails },
        Validators.required
      ),
      id_series: new FormControl({ value: this.driver ? this.driver.id_series : null, disabled: this.viewDetails }),
      national_id_number: new FormControl(
        { value: this.driver ? this.driver.national_id_number : null, disabled: this.viewDetails },
        Validators.required
      ),
      national_id_issued_by: new FormControl(
        { value: this.driver ? this.driver.national_id_issued_by : null, disabled: this.viewDetails },
        Validators.required
      ),

      national_id_issued_at: new FormControl(
        {
          value: this.driver ? this.datePipe.transform(this.driver.national_id_issued_at, 'yyyy/MM/dd') : null,
          disabled: this.viewDetails,
        },
        Validators.required
      ),
      national_id_expires_at: new FormControl(
        {
          value: this.driver ? this.datePipe.transform(this.driver.national_id_expires_at, 'yyyy/MM/dd') : null,
          disabled: this.viewDetails,
        },
        Validators.required
      ),
      national_identifier: new FormControl(
        { value: this.driver ? this.driver.national_identifier : null, disabled: this.viewDetails },
        Validators.required
      ),

      emergency_contact_name: new FormControl(
        { value: this.driver ? this.driver.emergency_contact_name : null, disabled: this.viewDetails },
        Validators.required
      ),
      emergency_contact_phone: new FormControl(
        { value: this.driver ? this.driver.emergency_contact_phone : null, disabled: this.viewDetails },
        Validators.required
      ),

      comments: new FormControl({ value: this.driver ? this.driver.comments : null, disabled: this.viewDetails }),
    });
  }

  onDocTypeSelect(value: DropdownOption): void {
    this.newDriverform.patchValue({ id_document_type: value.optionValue });
    this.label = value.displayName;
  }

  onSave(newDriverform: FormGroup): void {
    this.isSaving = true;
    this.modalRef = this.modalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: { action: 'save', message: 'Are you sure you want to save this driver?' },
    });
    this.modalRef.content.Confirm.subscribe(() => {
      this.saveNewDriver(newDriverform);
      this.modalRef?.hide();
      this.isSaving = false;
    });
    this.modalRef.content.Decline.subscribe(() => {
      this.modalRef?.hide();
      this.isSaving = false;
    });
  }

  //
  // onSave(newDriverform: FormGroup): void {
  //   this.isSaving = true;
  //   this.showConfirmModal(
  //     () => {
  //       this.saveDriverData(newDriverform);
  //       this.isSaving = false;
  //     },
  //     () => {
  //       this.isSaving = false;
  //     }
  //   );
  // }

  // private showConfirmModal(onConfirm: () => void, onDecline: () => void): void {
  //   this.modalRef = this.modalService.show(ConfirmModalComponent, {
  //     class: 'modal-sm',
  //     animated: true,
  //     initialState: { action: 'save', message: 'Are you sure you want to save this driver?' },
  //   });

  //   this.modalRef.content.Confirm.subscribe(() => {
  //     onConfirm();
  //     this.modalRef?.hide();
  //   });

  //   this.modalRef.content.Decline.subscribe(() => {
  //     onDecline();
  //     this.modalRef?.hide();
  //   });
  // }

  // private saveDriverData(newDriverform: FormGroup): void {
  //   this.authCoreService
  //     .getUserInfo()
  //     .pipe(takeUntilDestroyed(this.destroyRef))
  //     .subscribe({
  //       next: (user: any) => {
  //         const driver = this.createDriverForm(newDriverform, user);

  //         if (!this.driver) {
  //           this.addNewDriver(driver);
  //         } else {
  //           this.updateExistingDriver(driver);
  //         }
  //       },
  //     });
  // }

  // private createDriverForm(newDriverform: FormGroup, user: any): any {
  //   const driverData = {
  //     s_name: newDriverform.value.s_name,
  //     f_name: newDriverform.value.f_name,
  //     date_of_birth: this.datePipe.transform(newDriverform.value.date_of_birth, 'yyyy-MM-dd'),
  //     phone: newDriverform.value.phone,
  //     email: newDriverform.value.email,
  //     city: newDriverform.value.city,
  //     county: newDriverform.value.county,
  //     street: newDriverform.value.street,
  //     street_number: newDriverform.value.street_number,
  //     id_document_type: newDriverform.value.id_document_type,
  //     id_series: newDriverform.value.id_series,
  //     national_id_number: newDriverform.value.national_id_number,
  //     national_id_issued_by: newDriverform.value.national_id_issued_by,
  //     national_id_issued_at: this.datePipe.transform(newDriverform.value.national_id_issued_at, 'yyyy-MM-dd'),
  //     national_id_expires_at: this.datePipe.transform(newDriverform.value.national_id_expires_at, 'yyyy-MM-dd'),
  //     national_identifier: newDriverform.value.national_identifier,
  //     emergency_contact_name: newDriverform.value.emergency_contact_name,
  //     emergency_contact_phone: newDriverform.value.emergency_contact_phone,
  //     comments: newDriverform.value.comments,
  //   };

  //   if (!this.driver) {
  //     return {
  //       ...driverData,
  //       created_at: new Date().toISOString(),
  //       created_by_id: user.id,
  //       created_by_name: user.name,
  //     };
  //   } else {
  //     return {
  //       ...driverData,
  //       edited_by_id: user.id,
  //       edited_by_name: user.name,
  //     };
  //   }
  // }

  // private addNewDriver(driver: any): void {
  //   if (this.isValidForm(this.newDriverform)) {
  //     this.supabaseService.newDriver(driver).then((res) => {
  //       this.handleSupabaseResponse(res, 'Driver not added', 'Driver added successfully');
  //     });
  //   } else {
  //     this.toastr.error('Please fill in all the fields', 'Error!');
  //   }
  // }

  // private updateExistingDriver(driver: any): void {
  //   this.supabaseService.updateDriver(driver, this.driver.id).then((res) => {
  //     this.handleSupabaseResponse(res, 'Driver not updated', 'Driver updated successfully');
  //   });
  // }

  // private isValidForm(newDriverform: FormGroup): boolean {
  //   return newDriverform.valid;
  // }

  // private handleSupabaseResponse(res: any, errorTitle: string, successTitle: string): void {
  //   if (res.error) {
  //     this.toastr.error(`${res.error?.details}`, errorTitle);
  //   } else {
  //     this.toastr.success(successTitle, 'Success!');
  //     this.tableRefresh.emit();
  //     this.bsModalRef.hide();
  //   }
  // }

  //

  //write data to the DB on submit the form
  saveNewDriver(newDriverform: FormGroup) {
    console.log(newDriverform.value);
    this.authCoreService
      .getUserInfo()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (user: any) => {
          if (!this.driver) {
            if (newDriverform.valid) {
              const driver = {
                created_at: new Date().toISOString(),
                created_by_id: user.id,
                created_by_name: user.name,

                s_name: newDriverform.value.s_name,
                f_name: newDriverform.value.f_name,
                date_of_birth: this.datePipe.transform(newDriverform.value.date_of_birth, 'yyyy-MM-dd'),

                phone: newDriverform.value.phone,
                email: newDriverform.value.email,

                city: newDriverform.value.city,
                county: newDriverform.value.county,
                street: newDriverform.value.street,
                street_number: newDriverform.value.street_number,

                id_document_type: newDriverform.value.id_document_type,
                id_series: newDriverform.value.id_series,
                national_id_number: newDriverform.value.national_id_number,
                national_id_issued_by: newDriverform.value.national_id_issued_by,

                national_id_issued_at: this.datePipe.transform(newDriverform.value.national_id_issued_at, 'yyyy-MM-dd'),
                national_id_expires_at: this.datePipe.transform(
                  newDriverform.value.national_id_expires_at,
                  'yyyy-MM-dd'
                ),
                national_identifier: newDriverform.value.national_identifier,

                emergency_contact_name: newDriverform.value.emergency_contact_name,
                emergency_contact_phone: newDriverform.value.emergency_contact_phone,

                comments: newDriverform.value.comments,
              };
              this.supabaseService.newDriver(driver).then((res) => {
                if (res.error) {
                  this.toastr.error(`${res.error?.details}`, 'Driver not added');
                } else {
                  this.toastr.success('Driver added successfully', 'Success!');
                  this.tableRefresh.emit();
                }
              });
              this.bsModalRef.hide();
            } else {
              this.toastr.error('Please fill in all the fields', 'Error!');
            }
          } else {
            const driver = {
              edited_by_id: user.id,
              edited_by_name: user.name,

              s_name: newDriverform.value.s_name,
              f_name: newDriverform.value.f_name,
              date_of_birth: this.datePipe.transform(newDriverform.value.date_of_birth, 'yyyy-MM-dd'),

              phone: newDriverform.value.phone,
              email: newDriverform.value.email,

              city: newDriverform.value.city,
              county: newDriverform.value.county,
              street: newDriverform.value.street,
              street_number: newDriverform.value.street_number,

              id_document_type: newDriverform.value.id_document_type,
              id_series: newDriverform.value.id_series,
              national_id_number: newDriverform.value.national_id_number,
              national_id_issued_by: newDriverform.value.national_id_issued_by,

              national_id_issued_at: this.datePipe.transform(newDriverform.value.national_id_issued_at, 'yyyy-MM-dd'),
              national_id_expires_at: this.datePipe.transform(newDriverform.value.national_id_expires_at, 'yyyy-MM-dd'),
              national_identifier: newDriverform.value.national_identifier,

              emergency_contact_name: newDriverform.value.emergency_contact_name,
              emergency_contact_phone: newDriverform.value.emergency_contact_phone,

              comments: newDriverform.value.comments,
            };
            this.supabaseService.updateDriver(driver, this.driver.id).then((res) => {
              if (res.error) {
                this.toastr.error(`${res.error?.details}`, 'Driver not updated');
              } else {
                this.toastr.success('Driver updated successfully', 'Success!');

                this.tableRefresh.emit();
                this.bsModalRef.hide();
              }
            });
          }
        },
      });
  }

  download(driver: Driver) {
    this.isSaving = true;
    console.log(this.isSaving);
    this.toastr.info('The download of Driver Details has started', 'Info!');

    setTimeout(() => {
      this.savePDF(driver);
      this.toastr.success('Driver Details downloaded successfully', 'Success!');
      this.modalRef?.hide();
    }, 2000);
    this.isSaving = false;
  }

  savePDF(driver: Driver) {
    const doc = new jsPDF();

    doc.text('Profil sofer', 10, 10);
    doc.text('Nume: ' + driver.s_name, 10, 20);
    doc.text('Telefon: ' + driver.phone, 10, 30);
    doc.text('Email: ' + driver.email, 10, 40);
    doc.text('Data nasterii: ' + driver.date_of_birth, 10, 50);
    doc.text('Oras: ' + driver.city, 10, 60);
    doc.text('Judet: ' + driver.county, 10, 70);
    doc.text('Strada: ' + driver.street, 10, 80);
    doc.text('Numar strada: ' + driver.street_number, 10, 90);
    doc.text('Tip document: ' + driver.id_document_type, 10, 100);
    doc.text('Serie CI: ' + driver.id_series, 10, 110);
    doc.text('Numar CI: ' + driver.national_id_number, 10, 120);
    doc.text('Eliberat de: ' + driver.national_id_issued_by, 10, 130);
    doc.text('Eliberat la: ' + driver.national_id_issued_at, 10, 140);
    doc.text('Expira la: ' + driver.national_id_expires_at, 10, 150);
    doc.text('CNP: ' + driver.national_identifier, 10, 160);
    doc.text('Nume contact de urgenta: ' + driver.emergency_contact_name, 10, 170);
    doc.text('Telefon contact de urgenta: ' + driver.emergency_contact_phone, 10, 180);
    doc.text('Comentarii: ' + driver.comments, 10, 190);

    doc.save(`${driver.s_name}_${driver.f_name}-profile.pdf`);
  }
}

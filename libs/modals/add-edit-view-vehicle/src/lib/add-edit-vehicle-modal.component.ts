import { Component, DestroyRef, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { DatePipe, NgFor, NgIf } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from '@jurca-raul/modals/confirm';
import { ToastrService } from 'ngx-toastr';
import { ModalComponent } from '@jurca-raul/modals/modal';
import { ButtonComponent } from '@jurca-raul/components/button';
import { InputDirective, InputGroupComponent } from '@jurca-raul/components/form';
import { AuthCoreService } from '@jurca-raul/core-auth';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { DropdownComponent } from '@jurca-raul/components/dropdown';
import { VehiclesService } from '@jurca-raul/services/vehicle';
import { Vehicle } from '@jurca-raul/domains/vehicle';
import jsPDF from 'jspdf';

@Component({
  templateUrl: './add-edit-vehicle-modal.component.html',
  styleUrls: ['./add-edit-vehicle-modal.component.scss'],
  standalone: true,
  providers: [DatePipe],
  imports: [
    NgIf,
    NgFor,
    ReactiveFormsModule,
    FormsModule,
    ModalComponent,
    ButtonComponent,
    InputGroupComponent,
    InputDirective,
    BsDatepickerModule,
    DropdownComponent,
  ],
})
export class AddEditVehicleModalComponent {
  @Output() tableRefresh: EventEmitter<any> = new EventEmitter();

  newVehicleform: FormGroup = new FormGroup({});

  viewDetails: boolean = false;
  vehicles: Vehicle[] | null = [];
  vehicle!: Vehicle;

  user: any;

  // datepicker configurations
  bsDateConfig: Partial<BsDatepickerModule> = {
    dateInputFormat: 'DD/MM/YYYY',
    containerClass: 'theme-dark-blue',
    isAnimated: true,
    adaptivePosition: true,
  };

  modalRef?: BsModalRef;
  message?: string;

  constructor(
    private destroyRef: DestroyRef,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private modalService: BsModalService,
    public bsModalRef: BsModalRef,
    private vehicleService: VehiclesService,
    private toastr: ToastrService,
    private authCoreService: AuthCoreService
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.newVehicleform = this.formBuilder.group({
      brand: new FormControl(
        { value: this.vehicle ? this.vehicle.brand : null, disabled: this.viewDetails },
        Validators.required
      ),
      model: new FormControl(
        { value: this.vehicle ? this.vehicle.model : null, disabled: this.viewDetails },
        Validators.required
      ),
      registration_number: new FormControl(
        { value: this.vehicle ? this.vehicle.registration_number : null, disabled: this.viewDetails },
        Validators.required
      ),

      vin: new FormControl(
        { value: this.vehicle ? this.vehicle.vin : null, disabled: this.viewDetails },
        Validators.required
      ),
      date_of_fabrication: new FormControl(
        {
          value: this.vehicle ? this.datePipe.transform(this.vehicle.date_of_fabrication, 'yyyy/MM/dd') : null,
          disabled: this.viewDetails,
        },
        Validators.required
      ),

      technical_inspection_exp: new FormControl(
        {
          value: this.vehicle ? this.datePipe.transform(this.vehicle.technical_inspection_exp, 'yyyy/MM/dd') : null,
          disabled: this.viewDetails,
        },
        Validators.required
      ),
      assurance_exp: new FormControl(
        {
          value: this.vehicle ? this.datePipe.transform(this.vehicle.assurance_exp, 'yyyy/MM/dd') : null,
          disabled: this.viewDetails,
        },
        Validators.required
      ),

      is_available: new FormControl(
        { value: this.vehicle ? this.vehicle.is_available : false, disabled: this.viewDetails },
        Validators.required
      ),

      comments: new FormControl({ value: this.vehicle ? this.vehicle.comments : null, disabled: this.viewDetails }),
    });
  }

  onSave(newVehicleform: FormGroup): void {
    this.modalRef = this.modalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
    });
    this.modalRef.content.Confirm.subscribe(() => {
      this.saveNewDriver(newVehicleform);
      this.modalRef?.hide();
    });
    this.modalRef.content.Decline.subscribe(() => {
      this.modalRef?.hide();
    });
  }

  //write data to the DB on submit the form
  saveNewDriver(newVehicleform: FormGroup) {
    this.authCoreService
      .getUserInfo()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (user: any) => {
          if (!this.vehicle) {
            if (newVehicleform.valid) {
              const vehicle = {
                created_at: new Date().toISOString(),
                created_by_id: user.id,
                created_by_name: user.name,

                brand: newVehicleform.value.brand,
                model: newVehicleform.value.model,
                registration_number: newVehicleform.value.registration_number,
                vin: newVehicleform.value.vin,
                date_of_fabrication: this.datePipe.transform(newVehicleform.value.date_of_fabrication, 'yyyy-MM-dd'),
                technical_inspection_exp: this.datePipe.transform(
                  newVehicleform.value.technical_inspection_exp,
                  'yyyy-MM-dd'
                ),
                assurance_exp: this.datePipe.transform(newVehicleform.value.assurance_exp, 'yyyy-MM-dd'),
                is_available: true,
                comments: newVehicleform.value.comments,
              };
              this.vehicleService.newVehicle(vehicle).then((res) => {
                if (res.error) {
                  this.toastr.error(`${res.error?.details}`, 'Vehicle not added');
                } else {
                  this.toastr.success('Vehicle added successfully', 'Success!');
                  this.tableRefresh.emit();
                }
              });
              this.bsModalRef.hide();
            } else {
              this.toastr.error('Please fill in all the fields', 'Error!');
            }
          } else {
            const vehicle = {
              created_at: new Date().toISOString(),
              created_by_id: user.id,
              created_by_name: user.name,

              brand: newVehicleform.value.brand,
              model: newVehicleform.value.model,
              registration_number: newVehicleform.value.registration_number,
              vin: newVehicleform.value.vin,
              date_of_fabrication: this.datePipe.transform(newVehicleform.value.date_of_fabrication, 'yyyy-MM-dd'),
              technical_inspection_exp: this.datePipe.transform(
                newVehicleform.value.technical_inspection_exp,
                'yyyy-MM-dd'
              ),
              assurance_exp: this.datePipe.transform(newVehicleform.value.assurance_exp, 'yyyy-MM-dd'),
              is_available: newVehicleform.value.is_available,
              comments: newVehicleform.value.comments,
            };
            this.vehicleService.updateVehicle(vehicle, this.vehicle.id).then((res) => {
              if (res.error) {
                this.toastr.error(`${res.error?.details}`, 'Vehicle not updated');
              } else {
                this.toastr.success('Vehicle updated successfully', 'Success!');

                this.tableRefresh.emit();
                this.bsModalRef.hide();
              }
            });
          }
        },
      });
  }

  print(vehicle: Vehicle) {
    const doc = new jsPDF();

    doc.text('Vehicle' + vehicle.registration_number, 10, 10);
    doc.text('Brand: ' + vehicle.brand, 10, 20);
    doc.text('Model: ' + vehicle.model, 10, 30);
    doc.text('Plate Reg. No.: ' + vehicle.registration_number, 10, 40);
    doc.text('VIN: ' + vehicle.vin, 10, 50);
    doc.text('Fabrication date: ' + vehicle.date_of_fabrication, 10, 60);
    doc.text('Technical Inspection Exp: ' + vehicle.technical_inspection_exp, 10, 70);
    doc.text('Assurance Exo: ' + vehicle.assurance_exp, 10, 80);
    doc.text('Comments: ' + vehicle.comments, 10, 90);

    doc.save(`${vehicle.registration_number}_${vehicle.brand}_${vehicle.model}-vehicle_details.pdf`);
  }
}

import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'jrDatePipe',
  standalone: true,
})
export class JrDatePipe implements PipeTransform {
  transform(value: string | null): string {
    if (value === null) {
      return '';
    }

    const datePipe = new DatePipe('en-US');
    const transformedDate = datePipe.transform(value, 'dd/MM/yyyy');
    return transformedDate || '';
  }
}

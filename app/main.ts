import { bootstrapApplication, BrowserModule } from '@angular/platform-browser';
import { RootComponent } from '@jurca-raul/core';
import { APP_ROUTES } from './app.routes';
import { provideRouter, RouterModule } from '@angular/router';
import { importProvidersFrom } from '@angular/core';
import { AuthHttpInterceptor, AuthModule } from '@auth0/auth0-angular';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { environment as env } from './environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { SortableModule } from 'ngx-bootstrap/sortable';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ToasterModule } from 'angular2-toaster';
import { BrowserAnimationsModule, provideAnimations } from '@angular/platform-browser/animations';
import { provideToastr, ToastNoAnimationModule, ToastrModule } from 'ngx-toastr';

bootstrapApplication(RootComponent, {
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptor, multi: true },

    provideRouter(APP_ROUTES),
    provideToastr({
      timeOut: 5000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      closeButton: true,
      progressBar: true,
      extendedTimeOut: 2000,
    }),
    provideAnimations(),
    importProvidersFrom(
      // AuthModule.forRoot({
      //   ...env.auth,
      //   httpInterceptor: {
      //     ...env.httpInterceptor,
      //   },
      // }),

      AuthModule.forRoot({
        domain: 'rauljurca.eu.auth0.com',
        clientId: 'ScOH0TqIro6vrpm6bckt4riLdyw28NNt',

        authorizationParams: {
          redirect_uri: window.location.origin,

          // Request this audience at user authentication time
          audience: 'https://rauljurca.eu.auth0.com/api/v2/',

          // Request this scope at user authentication time
          scopes: ['read:users', 'read:current_user'],
        },

        // Specify configuration for the interceptor
        httpInterceptor: {
          allowedList: [
            {
              // Match any request that starts 'https://{yourDomain}/api/v2/' (note the asterisk)
              uri: 'https://rauljurca.eu.auth0.com/api/v2/',
              tokenOptions: {
                authorizationParams: {
                  // The attached token should target this audience
                  audience: 'https://rauljurca.eu.auth0.com/api/v2/',

                  // The attached token should have these scopes
                  scopes: ['read:users', 'read:current_user'],
                },
              },
            },
          ],
        },
      }),

      // BrowserModule,
      HttpClientModule,

      // BrowserAnimationsModule,

      BsDropdownModule.forRoot(),
      BsDatepickerModule.forRoot(),
      TimepickerModule.forRoot(),
      ModalModule.forRoot(),
      ButtonsModule.forRoot(),
      PopoverModule.forRoot(),
      AccordionModule.forRoot(),
      CollapseModule.forRoot(),
      SortableModule.forRoot(),
      TypeaheadModule.forRoot(),
      TooltipModule.forRoot(),
      TabsModule.forRoot(),
      ProgressbarModule.forRoot()
    ),
  ],
}).catch((err) => console.error(err));
